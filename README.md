# Imprimir documentos en nodejs con la librería "printer"
La liberìa "pinter", permite imprimir archivos desde la plataforma node tanto para sistemas operativos Unix y Windows

## Instalación
* Asegúrese de tener Python 2.x instalado en ts sistema. Los usuarios de Windows también necesitarán Visual Studio.
* Es necesario también instalar la librerìa node-gyp con el siguiente comando 
``` JavaScript
npm install -g node-gyp
```
* En caso  de presentar problemas con la instalacion de esta librería puede instalarla con los siguiente comando  si se esta en una equipo Linux
``` JavaScript
sudo apt-get install node-gyp
```
* Para los usuarios de Linux es necesario instalar la libreria libcups
``` JavaScript
sudo apt-get install libcups2-dev
```
* Luego ingresamos el siguiente comando para instalar la libreria "printer"
``` JavaScript
npm install printer
```

## Ejemplo

* Para probar el funcionamiento de la librerìa, vamos a implementar una función en modulo principal de nuestro proyecto, 

``` JavaScript
imprimir() {
    var printer = require("printer/lib")
    const filename = './prueba.pdf';
    const printerName = 'EPSON-L355-Series';
    console.log(printer)
    if (process.platform != 'win32') {
      printer.printFile({
        filename: filename,
        printer: printerName,
        success: function (jobID) {
          console.log("sent to printer with ID: " + jobID);
        },
        error: function (err) {
          console.log(err);
        }
      });
    } else {
      var fs = require('fs');
      printer.printDirect({
        data: fs.readFileSync(filename),
        printer: printerName,
        success: function (jobID) {
          console.log("sent to printer with ID: " + jobID);
        },
        error: function (err) {
          console.log(err);
        }
      });
    }
  }
  ```

  * donde es necesario importar la variable printer de la librería.
  ``` JavaScript
    var printer = require("printer/lib")
```

* luego creamos las constantes tanto para la ubicación del archivo a imprimir y otra para el nombre de la impresora. Eneste caso estas seran los valores de nuestras contantes:
``` JavaScript
    const filename = './prueba.pdf';
    const printerName = 'EPSON-L355-Series';
```
 * utlizamos la función .printFile la cual recibira como parámetros un objeto con el nombre del archivo (filename), el nombre de la impresora (printer).

``` JavaScript
    printer.printDirect({
        data: fs.readFileSync(filename),
        printer: printerName,
        success: function (jobID) {
          console.log("sent to printer with ID: " + jobID);
        },
        error: function (err) {
          console.log(err);
        }
      });
```
* Y como podemos ver se retornará una promesa que devolverá el id de la impresión en caso de que la impresion se ha dado correctamente. o devolcera un error encaso de tener algun problema con la impresión. 

![](./proyecto-imprimir-documento/imagenes/Selection_001.png)

<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>