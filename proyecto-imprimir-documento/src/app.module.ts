import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {
    this.imprimir();
  }
  imprimir() {
    var printer = require("printer/lib"),
      filename = './prueba.pdf';
    const printerName = 'EPSON-L355-Series';
    console.log(printer)
    if (process.platform != 'win32') {
      printer.printFile({
        filename: filename,
        printer: printerName,
        success: function (jobID) {
          console.log("sent to printer with ID: " + jobID);
        },
        error: function (err) {
          console.log(err);
        }
      });
    } else {
      // not yet implemented, use printDirect and text
      var fs = require('fs');
      printer.printDirect({
        data: fs.readFileSync(filename),
        printer: printerName,
        success: function (jobID) {
          console.log("sent to printer with ID: " + jobID);
        },
        error: function (err) {
          console.log(err);
        }
      });
    }
  }
}
